package net.eldiosantos.cloudstorage.dropbox.service.steps;

import cucumber.api.java8.En;
import net.eldiosantos.cloudstorage.config.StorageConfiguration;
import net.eldiosantos.cloudstorage.dropbox.service.DropboxDownloadService;
import org.junit.jupiter.api.Assertions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.nio.file.Paths;
import java.util.Optional;

/**
 * Created by esjunior on 10/02/2017.
 */
public class DownloadFileSteps implements En {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final StorageConfiguration config = StorageConfiguration.apply();

    private Optional<File> downloadedFile;

    public DownloadFileSteps() {
        try {
            When("^I look for the file \"([^\"]*)\"$", (String path) -> {
                try {
                    downloadedFile = new DropboxDownloadService(config).download(path);
                    logger.info("Downloadded file {}", downloadedFile.orElseGet(null));
                } catch (Exception e) {
                    logger.error("Error trying to download file '{}'", path, e);
                }
            });

            Then("^I have this file in my temporary folder$", () -> {
                Assertions.assertNotNull(downloadedFile, "Do we have a temporary file here?");
            });

            When("^I look for the file \"([^\"]*)\" to folder \"([^\"]*)\"$", (String remotePath, String localPath) -> {
                downloadedFile = new DropboxDownloadService(config).download(remotePath, localPath);
            });

            Then("^I have this file on the local filesystem \"([^\"]*)\"$", (String localPath) -> {
                final File file = downloadedFile.get();
                logger.info("Downloaded file actual location '{}'", downloadedFile!=null ? file.getAbsolutePath() : "null");
                logger.info("Downloaded file right location '{}'", localPath);
                Assertions.assertNotNull(file, "We have a downloaded file, right?");
                Assertions.assertTrue(file.exists(), "The downloaded file exists?");
                Assertions.assertTrue(file.getAbsolutePath().equals(Paths.get(localPath).toFile().getAbsolutePath()), "The file is in the right place?");
            });

            Then("^I don't have a file here$", () -> {
                logger.info("We have a file here? {}", downloadedFile.isPresent());
                Assertions.assertFalse(downloadedFile.isPresent(), "We had an exception type here?");
            });

        } catch (Exception e) {
            logger.error("Error defining test steps for download features...", e);
        }
    }
}
