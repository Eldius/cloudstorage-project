package net.eldiosantos.cloudstorage.api.service;

import java.io.File;
import java.util.Optional;

/**
 * Created by esjunior on 10/02/2017.
 */
public interface DownloadService {
    Optional<File> download(final String resource, final String dest);
    Optional<File> download(final String resource);
}
