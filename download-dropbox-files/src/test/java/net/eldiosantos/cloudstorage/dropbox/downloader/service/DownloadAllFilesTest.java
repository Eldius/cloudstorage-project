package net.eldiosantos.cloudstorage.dropbox.downloader.service;

import net.eldiosantos.cloudstorage.api.model.Resource;
import net.eldiosantos.cloudstorage.config.StorageConfiguration;
import net.eldiosantos.cloudstorage.dropbox.service.DropboxDownloadService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.Collection;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.stream.Collectors;

public class DownloadAllFilesTest {
    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final StorageConfiguration config = StorageConfiguration.apply();

    @Test
    @DisplayName("Downloading all my files...")
    public void test() throws InterruptedException {
        final String destFolder = "build/download_all_my_files";

        final ExecutorService executor = Executors.newWorkStealingPool();

        final Collection<Future<File>>executions = new LinkedBlockingQueue<>();
        final DropboxDownloadService downloadService = new DropboxDownloadService(config);
        new ExtendedDropboxListService(config).list("/", new ExtendedDropboxListService.ListingAction() {
            @Override
            public void process(Resource r) {
                logger.info("Adding '{}' for download stack", r.getPathDisplay());
                executions.add(executor.submit(() -> downloadService.download(r, destFolder)));
            }
        });

        while (executions.size() > 0) {
            executions.removeAll(
                executions.parallelStream()
                    .filter(Future::isDone)
                    .collect(Collectors.toSet())
            );
            Thread.sleep(1000l);
        }
    }
}
