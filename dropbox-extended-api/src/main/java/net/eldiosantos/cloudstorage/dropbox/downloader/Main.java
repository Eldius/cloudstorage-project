package net.eldiosantos.cloudstorage.dropbox.downloader;

import net.eldiosantos.cloudstorage.config.StorageConfiguration;
import net.eldiosantos.cloudstorage.dropbox.service.DropboxListService;

public class Main {

/*
export DROPBOX_APPKEY="1y5h1xogul75mh4"
export DROPBOX_APPSECRET="5gbf1euvd0ezqo6"
export DROPBOX_ACCESSTOKEN="6KTGo87Cs1QAAAAAAAB5KrTT9lwAWp-Un1wJQTJ259CYDW84_4AHeyiOapIRi-Xh"
 */

    private final StorageConfiguration config = StorageConfiguration.apply();

    public void main(String ... params) {
        new Main().start();
    }

    public void start() {
        new DropboxListService(config).list("/");
    }
}
