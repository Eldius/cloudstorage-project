package net.eldiosantos.cloudstorage.dropbox.downloader.service;

import net.eldiosantos.cloudstorage.api.model.Resource;
import net.eldiosantos.cloudstorage.config.StorageConfiguration;
import net.eldiosantos.cloudstorage.dropbox.pojo.ListFoldersRequest;
import net.eldiosantos.cloudstorage.dropbox.service.DropboxListService;
import net.eldiosantos.cloudstorage.dropbox.service.request.DropboxRequestClient;

public class ExtendedDropboxListService extends DropboxListService {

    private final DropboxRequestClient client;

    public ExtendedDropboxListService(StorageConfiguration config) {
        super(config);
        this.client = new DropboxRequestClient(StorageConfiguration.apply().dropbox());
    }

    public void list(final ListFoldersRequest request, final ListingAction action) {
        try {
            stream(request)
                .forEach(action::process);
        } catch (Exception e) {
            throw new IllegalStateException(String.format("Error trying to list resources from Dropbox at %s", request.getPath()), e);
        }

    }

    public void list(final String path, final ListingAction action) {
        this.list(generateDefaultRequest(path), action);
    }

    public interface ListingAction {
        default void process(Resource resource) {
            System.out.println(new StringBuffer(resource.getPathDisplay()).append("/").append(resource.getName()).toString());
        }
    }
}
