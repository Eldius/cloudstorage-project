package net.eldiosantos.cloudstorage.dropbox.downloader.service;

import net.eldiosantos.cloudstorage.api.model.Resource;
import net.eldiosantos.cloudstorage.config.StorageConfiguration;
import net.eldiosantos.cloudstorage.dropbox.service.DropboxDownloadService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

class ExtendedDropboxListServiceTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final StorageConfiguration config = StorageConfiguration.apply();

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    private class TestListingActionImplementation implements ExtendedDropboxListService.ListingAction {

    }

    @Test
    @DisplayName("Testing the new 'Extended' interface model.")
    public void listFunctionalInterfaceWithDefaultMethod() {
        final AtomicLong counter = new AtomicLong(0);
        new ExtendedDropboxListService(config).list("", new TestListingActionImplementation());
    }

    @Test
    @DisplayName("Testing the new 'Extended' interface model.")
    public void list() {
        final AtomicLong counter = new AtomicLong(0);
        new ExtendedDropboxListService(config).list("", new ExtendedDropboxListService.ListingAction() {
            @Override
            public void process(Resource resource) {
                logger.info("resource: {}", resource.getPathDisplay());
                counter.getAndIncrement();
            }
        });

        logger.info("returned '{}' files.", counter.get());
        assertEquals(14, counter.get(), "Do we have 14 files?");
    }

    @Test
    @DisplayName("Testing the new 'Extended' interface model again (download .txt files).")
    public void downloadTxtFiles() throws IOException {
        final AtomicLong counter = new AtomicLong(0);
        new ExtendedDropboxListService(config).list("", new ExtendedDropboxListService.ListingAction() {
            @Override
            public void process(Resource resource) {
                if(resource.getType().equals(Resource.ResourceType.FILE) && resource.getName().endsWith(".txt")) {
                    logger.info("resource: {}", resource.getPathDisplay());
                    counter.getAndIncrement();
                    new DropboxDownloadService(config).download(resource, "build/download_onlu_txt_files");
                }
            }
        });

        final Set<Path> files = Files.walk(Paths.get("build/download_onlu_txt_files"))
                .filter(path -> path.toFile().isFile())
                .collect(Collectors.toSet());

        logger.info("returned '{}' txt files.", counter.get());

        assertEquals(2, counter.get(), "Do we found '2' .txt files?");

        assertEquals(2, files.size(), "Do we have '2' files on the destination folder?");
    }
}
